# ZuluFX does not run on Alpine, hence resorting to Debian
FROM debian:testing

# the Java version has to be passed
ARG JAVA_VERSION=8

# install prerequisites to download Gradle and ZuluFX
RUN echo "Building for JDK $JAVA_VERSION" && \ 
    apt-get update -yq && \ 
    apt-get install --no-install-recommends -y wget curl ca-certificates unzip git && \ 
    rm -rf /var/lib/apt/lists/*

# download and install ZuluFX
RUN ZULUFXURL=https://cdn.azul.com/zulu/bin/ && \ 
    ZULUFXFILE=`curl -s $ZULUFXURL | grep -oP "<a\s*href=\"[^\"]*\">\K[^<]*" | grep "fx-jdk$JAVA_VERSION[^-]*-linux_x64.tar.gz" | tail -1` && \ 
    echo "Fetching remote file $ZULUFXFILE" && \ 
    mkdir -p /opt/java && cd /opt/java && \
    wget -O zulufx.tar.gz "$ZULUFXURL$ZULUFXFILE" && \
    tar -xzf zulufx.tar.gz && rm zulufx.tar.gz && \
    mv zulu* current

# download and install Gradle
RUN GRADLEURL=https://services.gradle.org/distributions/ && \ 
    GRADLEFILE=`curl -s $GRADLEURL | grep -oP "<span\s*class=\"name\">\K[^<]*-bin.zip" | head -1` && \ 
    mkdir -p /opt/gradle && cd /opt/gradle && \ 
    echo "Fetching remote file $GRADLEFILE" && \ 
    wget -O gradle.zip "$GRADLEURL$GRADLEFILE" && \ 
    unzip gradle.zip && \ 
    rm gradle.zip && \ 
    mv gradle* current

# adjust environment variables
ENV JAVA_HOME "/opt/java/current"
ENV PATH "$JAVA_HOME/bin:/opt/gradle/current/bin:$PATH"

# test the installation
RUN java -version && \
    gradle --version

# default to bash shell
CMD ["bash"]
