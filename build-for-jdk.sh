#!/bin/sh
IMAGETAG="jdk$1-`date +%Y-%m-%d-%H-%M`-$CI_COMMIT_SHORT_SHA"
LATESTTAG="jdk$1-latest"
docker pull $RELEASE_IMAGE: || true
docker build --cache-from $RELEASE_IMAGE:$LATESTTAG --tag $RELEASE_IMAGE:$IMAGETAG \
  --tag $RELEASE_IMAGE:$LATESTTAG --build-arg JAVA_VERSION=$1 .
docker push $RELEASE_IMAGE:$IMAGETAG
docker push $RELEASE_IMAGE:$LATESTTAG
