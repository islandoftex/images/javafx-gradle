# A JavaFX docker image

This image is intended to build the Java applications of The Island of TeX
against. Hence, it bundles a JavaFX-enabled JDK (currently ZuluFX) and Gradle.

Feel free to use this image in your own environment but be warned that as
requirements of our projects may change so may this image.

## Licensing

The software in terms of the MIT license are the Dockerfiles, shell scripts and
test files provided. This does not include the pre-built Docker images we
provide. They follow the redistribution conditions of the bundled software.
